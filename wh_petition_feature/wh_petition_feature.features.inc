<?php
/**
 * @file
 * wh_petition_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function wh_petition_feature_node_info() {
  $items = array(
    'whitehouse_petition' => array(
      'name' => t('Whitehouse Petition'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'whitehouse_petition_signature' => array(
      'name' => t('Whitehouse Petition Signature'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
